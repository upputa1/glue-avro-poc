### AWS Kinesis Flink with AVRO serialization and de-serilzation

This POC showcases Avro integration with Flink. 

- `avro-maven-plugin` is configured to generate `Event` pojo from Avro Schema located under `resources`.
- `PutRecordGetRecordExample` shows how to serialize Avro records and publish on to the Kinesis stream.
- `BasicStreamingJob` is a Flink job. This class uses Avro deserializer and serializers for input and outputstreams respectively.








    
      
 
