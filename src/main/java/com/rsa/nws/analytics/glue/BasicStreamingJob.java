package com.rsa.nws.analytics.glue;

import com.amazonaws.services.kinesisanalytics.flink.connectors.config.AWSConfigConstants;
import com.amazonaws.services.schemaregistry.utils.AWSSchemaRegistryConstants;
import com.amazonaws.services.schemaregistry.utils.AvroRecordType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.rsa.nws.analytics.Event;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import org.apache.avro.Schema;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kinesis.FlinkKinesisConsumer;
import org.apache.flink.streaming.connectors.kinesis.FlinkKinesisProducer;
import org.apache.flink.streaming.connectors.kinesis.KinesisPartitioner;
import org.apache.flink.streaming.connectors.kinesis.config.ConsumerConfigConstants;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.types.Row;

/**
 * A basic Kinesis Data Analytics for Java application with Kinesis data streams as source and
 * sink.
 */
public class BasicStreamingJob {

  private static final String region = "us-east-1";
  private static final String inputStreamName = "NWS-anu-test1";
  //  private static final String inputStreamName = "NWS-anu-inputstream";
  private static final String outputStreamName = "NWS-anu-outputstream";
  static final String AVRO_USER_SCHEMA_FILE = "/Events.avsc";
  static final String REGISTRY_NAME = "analytics-poc";
  static final String SCHEMA_NAME = "Events";

  static MapFunction<Row, String> rowSerializer =
      row -> {
        Map<String, Object> map = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        Set<String> names = row.getFieldNames(true);
        for (String fieldName : names) {
          map.put(fieldName, row.getField(fieldName));
        }
        return mapper.writeValueAsString(map);
      };

  public GlueSchemaRegistryAvroDeserializationSchema<Event> getGlueDeserializingSchemaSpecific()
      throws Exception {
    Map<String, Object> configs = new HashMap<>();
    configs.put(AWSSchemaRegistryConstants.AWS_REGION, "us-east-1");
    configs
        .put(AWSSchemaRegistryConstants.AVRO_RECORD_TYPE, AvroRecordType.GENERIC_RECORD.getName());
    return GlueSchemaRegistryAvroDeserializationSchema.forSpecific(Event.class, configs);
  }

  public GlueSchemaRegistryAvroSerializationSchema<Event> getSerializingSchemaGeneric()
      throws Exception {
    Schema.Parser parser = new Schema.Parser();
    InputStream is = this.getClass().getResourceAsStream(AVRO_USER_SCHEMA_FILE);
    Schema schema = parser.parse(is);
    Map<String, Object> configs = new HashMap<>();
    configs.put(AWSSchemaRegistryConstants.AWS_REGION, "us-east-1");
    configs
        .put(AWSSchemaRegistryConstants.AVRO_RECORD_TYPE, AvroRecordType.GENERIC_RECORD.getName());
    return GlueSchemaRegistryAvroSerializationSchema.forSpecific(Event.class, outputStreamName, configs);
  }

  public static void main(String[] args) throws Exception {
    // set up the streaming execution environment
    final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
    BasicStreamingJob basicStreamingJobGlueSchema = new BasicStreamingJob();

    org.apache.flink.table.api.bridge.java.StreamTableEnvironment tableEnv = org.apache.flink.table.api.bridge.java.StreamTableEnvironment
        .create(
            env, EnvironmentSettings.newInstance().useBlinkPlanner().build());

    Properties consumerConfig = new Properties();
    consumerConfig.put(AWSConfigConstants.AWS_REGION, "us-east-1");
//    consumerConfig.put(ConsumerConfigConstants.STREAM_INITIAL_POSITION, "TRIM_HORIZON");
    consumerConfig.put(ConsumerConfigConstants.STREAM_INITIAL_POSITION, "LATEST");
    System.out.println("Gluejob schema started with input: " + inputStreamName);


//    final FlinkKinesisConsumer<Event> consumer = new FlinkKinesisConsumer<>(
//        inputStreamName
//        , basicStreamingJobGlueSchema.getGlueDeserializingSchemaSpecific(), consumerConfig);

        final FlinkKinesisConsumer<Event> consumer = new FlinkKinesisConsumer<>(
        inputStreamName
        , basicStreamingJobGlueSchema.getGlueDeserializingSchemaSpecific(), consumerConfig);

    Properties outputProperties = new Properties();
    outputProperties.setProperty(ConsumerConfigConstants.AWS_REGION, region);

    FlinkKinesisProducer<String> sink = new FlinkKinesisProducer(
        new SimpleStringSchema(),
        outputProperties);
    sink.setDefaultStream(outputStreamName);
    sink.setCustomPartitioner(new KinesisPartitioner<String>() {
      @Override
      public String getPartitionId(String event) {
        return UUID.randomUUID().toString();
      }
    });

    DataStream<Event> events = env.addSource(consumer);

    tableEnv.createTemporaryView("events", events);

    String sql = "select domain, event_time, ip_src, ip_dst from events where domain = 'good-domain.com'";

    tableEnv.toDataStream(tableEnv.sqlQuery(sql), Row.class).map(rowSerializer).addSink(sink);

    env.execute();

  }
}
