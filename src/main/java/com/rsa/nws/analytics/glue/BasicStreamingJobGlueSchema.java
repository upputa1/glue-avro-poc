package com.rsa.nws.analytics.glue;

import com.amazonaws.services.kinesisanalytics.flink.connectors.config.AWSConfigConstants;
import com.amazonaws.services.schemaregistry.utils.AWSSchemaRegistryConstants;
import com.amazonaws.services.schemaregistry.utils.AvroRecordType;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kinesis.FlinkKinesisConsumer;
import org.apache.flink.streaming.connectors.kinesis.FlinkKinesisProducer;
import org.apache.flink.streaming.connectors.kinesis.KinesisPartitioner;
import org.apache.flink.streaming.connectors.kinesis.config.ConsumerConfigConstants;
import org.apache.flink.table.api.EnvironmentSettings;
import software.amazon.awssdk.services.glue.model.Compatibility;

/**
 * A basic Kinesis Data Analytics for Java application with Kinesis data streams as source and
 * sink.
 */
public class BasicStreamingJobGlueSchema {

  private static final String region = "us-east-1";
  private static final String inputStreamName = "NWS-anu-test1";
  //  private static final String inputStreamName = "NWS-anu-inputstream";
  private static final String outputStreamName = "NWS-anu-outputstream";
  static final String AVRO_USER_SCHEMA_FILE = "/Events.avsc";
  static final String REGISTRY_NAME = "analytics-poc";
  static final String SCHEMA_NAME = "Events";

  public GlueSchemaRegistryAvroDeserializationSchema<GenericRecord> getDeserializingSchemaGeneric()
      throws Exception {
    Schema.Parser parser = new Schema.Parser();
    InputStream is = this.getClass().getResourceAsStream(AVRO_USER_SCHEMA_FILE);
    Schema schema = parser.parse(is);

    Map<String, Object> configs = new HashMap<>();
    configs.put(AWSSchemaRegistryConstants.AWS_REGION, "us-east-1");
    configs
        .put(AWSSchemaRegistryConstants.AVRO_RECORD_TYPE, AvroRecordType.GENERIC_RECORD.getName());
//    configs.put(AWSSchemaRegistryConstants.SCHEMA_AUTO_REGISTRATION_SETTING, true);
    configs.put(AWSSchemaRegistryConstants.REGISTRY_NAME, REGISTRY_NAME);
    configs.put(AWSSchemaRegistryConstants.SCHEMA_NAME, SCHEMA_NAME);
//    configs.put(AWSSchemaRegistryConstants.CACHE_TIME_TO_LIVE_MILLIS, "2000");
    configs.put(AWSSchemaRegistryConstants.COMPATIBILITY_SETTING, Compatibility.FULL);
    return GlueSchemaRegistryAvroDeserializationSchema.forGeneric(schema, configs);
  }

  public GlueSchemaRegistryAvroSerializationSchema<GenericRecord> getSerializingSchemaGeneric()
      throws Exception {
    Schema.Parser parser = new Schema.Parser();
    InputStream is = this.getClass().getResourceAsStream(AVRO_USER_SCHEMA_FILE);
    Schema schema = parser.parse(is);
    Map<String, Object> configs = new HashMap<>();
    configs.put(AWSSchemaRegistryConstants.AWS_REGION, "us-east-1");
    configs
        .put(AWSSchemaRegistryConstants.AVRO_RECORD_TYPE, AvroRecordType.GENERIC_RECORD.getName());
//    configs.put(AWSSchemaRegistryConstants.SCHEMA_AUTO_REGISTRATION_SETTING, true);
    configs.put(AWSSchemaRegistryConstants.REGISTRY_NAME, REGISTRY_NAME);
    configs.put(AWSSchemaRegistryConstants.SCHEMA_NAME, SCHEMA_NAME);
//    configs.put(AWSSchemaRegistryConstants.CACHE_TIME_TO_LIVE_MILLIS, "2000");
    configs.put(AWSSchemaRegistryConstants.COMPATIBILITY_SETTING, Compatibility.FULL);
    return GlueSchemaRegistryAvroSerializationSchema.forGeneric(schema, outputStreamName, configs);
  }

  public static void notmain(String[] args) throws Exception {
    // set up the streaming execution environment
    final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
    BasicStreamingJobGlueSchema basicStreamingJobGlueSchema = new BasicStreamingJobGlueSchema();


    Properties consumerConfig = new Properties();
    consumerConfig.put(AWSConfigConstants.AWS_REGION, "us-east-1");
//    consumerConfig.put(ConsumerConfigConstants.STREAM_INITIAL_POSITION, "TRIM_HORIZON");
    consumerConfig.put(ConsumerConfigConstants.STREAM_INITIAL_POSITION, "LATEST");
    System.out.println("Gluejob schema started with input: " + inputStreamName);

    consumerConfig.put(AWSSchemaRegistryConstants.AVRO_RECORD_TYPE,
        AvroRecordType.GENERIC_RECORD.getName()); // Only required for AVRO data format

    consumerConfig.put(AWSSchemaRegistryConstants.SCHEMA_AUTO_REGISTRATION_SETTING, true);
    consumerConfig.put(AWSSchemaRegistryConstants.COMPATIBILITY_SETTING, Compatibility.FULL_ALL);
//    consumerConfig.put(AWSSchemaRegistryConstants.CACHE_TIME_TO_LIVE_MILLIS, 2000);

    final FlinkKinesisConsumer<GenericRecord> consumer = new FlinkKinesisConsumer<GenericRecord>(
        inputStreamName
        , basicStreamingJobGlueSchema.getDeserializingSchemaGeneric(), consumerConfig);

    Properties outputProperties = new Properties();
    outputProperties.setProperty(ConsumerConfigConstants.AWS_REGION, region);

    FlinkKinesisProducer<GenericRecord> sink = new FlinkKinesisProducer(
        basicStreamingJobGlueSchema.getSerializingSchemaGeneric(),
        outputProperties);
    sink.setDefaultStream(outputStreamName);
    sink.setCustomPartitioner(new KinesisPartitioner<GenericRecord>() {
      @Override
      public String getPartitionId(GenericRecord event) {
        return UUID.randomUUID().toString();
      }
    });

    env.addSource(consumer).filter((record) ->
        {
          System.out.println("fetched record: " + record.toString());
          return record.get("domain").equals("good-domain.com");
        }
    ).addSink(sink);


    env.execute();

  }
}
